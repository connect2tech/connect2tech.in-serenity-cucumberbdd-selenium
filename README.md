connect2tech.in-Cucumber-BDD
============================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.


# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


# Important Links
- [Bitbucket Repositories](https://bitbucket.org/connect2tech)
- [Serenity Documentation](http://thucydides.info/docs/serenity/#introduction)

# How to use the project

## Setting up the project locally
~~1. Take the project from share drive. It also contains the jar files in jar-files
2. Import the project in eclipse
3. Delete existing entries from .classpath file i.e delete entries of all the jar files.
4. Include the jar file in classpath from jar-files folder~~

```
- Go to your .m2 directory and take back up of repository folder.
- I have already given access to "shared". Access the folder and take repository.zip. Unzip the file and replace current repository with this folder.
- Re-run the maven command.
$ mvn dependency:resolve eclipse:eclipse
$ mvn clean test verify
```


