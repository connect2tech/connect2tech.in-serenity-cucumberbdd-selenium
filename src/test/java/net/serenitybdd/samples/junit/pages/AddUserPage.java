package net.serenitybdd.samples.junit.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://localhost:8888/connect2tech.in-Selenium-Automation-Java-1.x/users/add")
public class AddUserPage extends PageObject {

	@FindBy(id = "name")
	WebElement name;
	
	@FindBy(id = "email")
	WebElementFacade email;
	
	final static Logger logger = Logger.getLogger(AddUserPage.class);
	
	public void methods() {
		
		email.isVisible();
		email.isCurrentlyVisible();
		email.shouldBeVisible();
		email.isEnabled();
		email.shouldBeEnabled();
	}

	public void enterName(String name1) {
		
		
		
		name.clear();
		name.sendKeys(name1);
	}
	
	public void enterEmail(String email1) {
		email.clear();
		email.sendKeys(email1);
	}
}
