package net.serenitybdd.samples.junit.features.searching;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.samples.junit.steps.AddUserSteps;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

// tag::header[]
@RunWith(SerenityRunner.class)
public class WhenAddingUser {

	@Managed // <1>
	WebDriver driver;

	//AddUserPage addUserPage; // <1>
	@Steps
	AddUserSteps addUserStep;

	@Test
	public void addingUserDetails() {

		System.out
				.println("shouldInstantiatedPageObjectsForAWebTest================>" + Thread.currentThread().getId());

		/*addUserPage.open(); // <2>

		addUserPage.enterName("NC");
		addUserPage.enterEmail("nc@gmail.com");

		assertThat(addUserPage.getTitle()).contains("connect2tech");*/
		
		addUserStep.enterEmail();
		addUserStep.enterName();
	}

}
// end::endTest[]
