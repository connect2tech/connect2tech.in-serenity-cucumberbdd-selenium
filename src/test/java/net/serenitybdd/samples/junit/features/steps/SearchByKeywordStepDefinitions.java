package net.serenitybdd.samples.junit.features.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// tag::header[]
public class SearchByKeywordStepDefinitions {
	@Given("^I want to buy a wool scarf$")
	public void i_want_to_buy_a_wool_scarf() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@When("^I search for items containing 'wool'$")
	public void i_search_for_items_containing_wool() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@Then("^I should only see items related to 'wool'$")
	public void i_should_only_see_items_related_to_wool() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}
	
    @When("I filter results by type '(.*)'")
    public void filterResultsBy(String type) {
        //buyer.filters_results_by_type(type);
    }
    
    @Then("I should only see items containing '(.*)' of type '(.*)'")
    public void shouldSeeMatchingFilteredResults(String keyword, String type) {
        //buyer.should_see_items_related_to(keyword);
        //buyer.should_see_items_of_type(type);
    }
    
    @Given("I have searched for items containing '(.*)'")
    public void buyerHasSearchedFor(String keyword) {
       /* searchTerm = keyword;
        buyer.opens_etsy_home_page();
        buyer.searches_for_items_containing(keyword);
        buyer.filters_by_local_region();*/
    }


}
// end::tail[]
