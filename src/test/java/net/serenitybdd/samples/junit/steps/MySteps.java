package net.serenitybdd.samples.junit.steps;

import net.thucydides.core.annotations.Step;

// tag::classbody[]
public class MySteps {


    @Step("This Is Given Step")             // <2>
    public void this_Is_Given_Step() {
    }
    
    @Step("This Is When Step")             // <2>
    public void this_Is_When_Step() {
    }
    
    @Step("This Is Then Step")             // <2>
    public void this_Is_Then_Step() {
    }

   
}
// end::classbody[]
