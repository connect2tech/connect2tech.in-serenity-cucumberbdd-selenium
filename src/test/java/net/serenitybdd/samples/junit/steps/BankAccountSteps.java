package net.serenitybdd.samples.junit.steps;

import net.thucydides.core.annotations.Step;

// tag::classbody[]
public class BankAccountSteps {


    @Step("Open Bank Accout")             // <2>
    public void open_bank_account() {
    }
    
    @Step("Deposit INR {0} In Bank Account")             // <2>
    public void deposit_INR_in_bank_account(int amount) {
    }
    
    @Step("Check Balance In Bank Account")             // <2>
    public void check_balance_in_bank_account() {
    }

   
}
// end::classbody[]
