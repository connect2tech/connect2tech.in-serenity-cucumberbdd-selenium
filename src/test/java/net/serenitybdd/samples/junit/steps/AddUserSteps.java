package net.serenitybdd.samples.junit.steps;

import net.serenitybdd.samples.junit.pages.AddUserPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class AddUserSteps extends ScenarioSteps {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	AddUserPage addUserPage;

	@Step
	public void enterName() {
		System.out.println("enterName==>"+addUserPage.hashCode());
	}

	@Step
	public void enterEmail() {
		System.out.println("enterEmail==>"+addUserPage.hashCode());
	}

}
