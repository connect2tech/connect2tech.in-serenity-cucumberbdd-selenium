package in.connect2tech.book.chapter.steps;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.samples.junit.steps.MySteps;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class) // <1>
public class Feature1 {

	@Steps
	MySteps mySteps;

	@Test
	public void scenario1() {
		mySteps.this_Is_Given_Step();
		mySteps.this_Is_When_Step();
		mySteps.this_Is_Then_Step();
	}

	@Test
	public void scenario2() {
		mySteps.this_Is_Given_Step();
		mySteps.this_Is_When_Step();
		mySteps.this_Is_Then_Step();

	}
}
// end::testcase[]
