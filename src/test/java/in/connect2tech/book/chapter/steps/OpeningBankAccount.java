package in.connect2tech.book.chapter.steps;

import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.samples.junit.steps.BankAccountSteps;
import net.serenitybdd.samples.junit.steps.MySteps;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class) // <1>
public class OpeningBankAccount {

	@Steps
	BankAccountSteps bankAccountSteps;

	@Test
	public void open_account_with_zero_balance() {
		bankAccountSteps.open_bank_account();
		bankAccountSteps.deposit_INR_in_bank_account(0);
		bankAccountSteps.check_balance_in_bank_account();
	}

	@Test
	public void open_account_with_minimum_balance() {
		bankAccountSteps.open_bank_account();
		bankAccountSteps.deposit_INR_in_bank_account(5000);
		bankAccountSteps.check_balance_in_bank_account();
	}
}
// end::testcase[]
